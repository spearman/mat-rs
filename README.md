# `mat`

> dynamic, 2d, Vec-backed storage with rectangular iterators and
> in-place resizing

Interface and low-level code stolen from `vec2d`.


## Why this library?

This library was created to use as a rectangular 'window' container
for curses-like GUI (or TUI) in graphical applications, after
considering the currently available options on crates.io:

- `vec_2d::Vec2d<T>`, `vec2d::Vec2D<T>` -- incomplete implementations
- `ndarray::Array <T, (Ix,Ix)>` -- no in-place resizing, more complex
  underlying representation
- `nalgebra::DMatrix<T>` -- no rectangular slicing, no in-place
  resizing

`vec_2d` and `vec2d` provide only incomplete "generic"
implementations, while `ndarray` and `nalgebra` are more geared
towards numerical applications, though they offer more functionality,
neither fits the exact needs of this library.
