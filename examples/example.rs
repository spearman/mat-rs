use mat::*;

fn main () {
  println!("mat example: main...");
  //
  //  small
  //
  let m = Mat::from_vec (
    (3, 4).into(),
    vec!['a','b','c','d','e','f','g','h','i','j','k','l']
  ).unwrap();
  println!("{}", m);

  println!("rect iter top...");
  let top = m.top().unwrap();
  for (Coordinate{row,column}, e) in m.rect_iter (top).unwrap() {
    println!("coord: ({},{})  elem: {}", row, column, e);
  }

  println!("rect iter bottom...");
  let bottom = m.bottom().unwrap();
  for (Coordinate{row,column}, e) in m.rect_iter (bottom).unwrap() {
    println!("coord: ({},{})  elem: {}", row, column, e);
  }

  println!("rect iter left...");
  let left = m.left().unwrap();
  for (Coordinate{row,column}, e) in m.rect_iter (left).unwrap() {
    println!("coord: ({},{})  elem: {}", row, column, e);
  }

  println!("rect iter right...");
  let right = m.right().unwrap();
  for (Coordinate{row,column}, e) in m.rect_iter (right).unwrap() {
    println!("coord: ({},{})  elem: {}", row, column, e);
  }

  println!("rect iter column 1...");
  let column1 = m.column (1).unwrap();
  for (Coordinate{row,column}, e) in m.rect_iter (column1).unwrap() {
    println!("coord: ({},{})  elem: {}", row, column, e);
  }

  println!("rect iter row 1...");
  let row1 = m.row (1).unwrap();
  for (Coordinate{row,column}, e) in m.rect_iter (row1).unwrap() {
    println!("coord: ({},{})  elem: {}", row, column, e);
  }

  //
  //  bigger
  //
  let mut m = Mat::<char>::default();

  m.resize ((8, 15).into(), 'o');
  println!("{}", m);

  println!("rect iter mut top...");
  let top = m.top().unwrap();
  for (_, e) in m.rect_iter_mut (top).unwrap() {
    *e = 'X';
  }

  println!("{}", m);

  println!("rect iter mut lower right block...");
  let block = Rectangle::from ((
    (4, 11).into(),
    (m.height()-1, m.width()-1).into()));

  for (_, e) in m.rect_iter_mut (block).unwrap() {
    *e = 'I';
  }

  println!("{}", m);

  println!("rect iter mut column 2...");
  let col2 = m.column (2).unwrap();

  for (_, e) in m.rect_iter_mut (col2).unwrap() {
    *e = 'E';
  }

  println!("{}", m);

  println!("rect iter mut middle box...");
  let rect = Rectangle::from (((3, 2).into(), (5, 3).into()));
  for (_, e) in m.rect_iter_mut (rect).unwrap() {
    *e = 'M';
  }

  println!("{}", m);

  println!("mat example: ...main");
}
