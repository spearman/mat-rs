#[cfg(feature="serde")]
use serde::{Deserialize, Serialize};

use super::Dimensions;

/// Row-major coordinate.
///
/// ```
/// # use mat::{Coordinate, Dimensions};
/// let c = Coordinate::from ((1, 2));
/// assert_eq!(c.row,    1);
/// assert_eq!(c.column, 2);
/// let c = Coordinate::from ([1, 2]);
/// assert_eq!(c.row,    1);
/// assert_eq!(c.column, 2);
/// let a = Coordinate::from ((1, 2));
/// let b = Dimensions::from ((1, 1));
/// assert_eq!(a + b, (2, 3).into());
/// let a = Coordinate::from ((1, 2));
/// let b = Dimensions::from ((1, 1));
/// assert_eq!(a - b, (0, 1).into());
/// ```
#[cfg_attr(feature = "serde", derive(Deserialize, Serialize))]
#[derive(Clone, Copy, Debug, Default, Eq, PartialEq)]
pub struct Coordinate {
  pub row    : usize,
  pub column : usize
}

impl From <(usize, usize)> for Coordinate {
  fn from ((row, column) : (usize, usize)) -> Self {
    Coordinate { row, column }
  }
}
impl From <[usize; 2]> for Coordinate {
  fn from ([row, column] : [usize; 2]) -> Self {
    Coordinate { row, column }
  }
}
impl From <Coordinate> for (usize, usize) {
  fn from (Coordinate { row, column } : Coordinate) -> Self {
    (row, column)
  }
}
impl From <Coordinate> for [usize; 2] {
  fn from (Coordinate { row, column } : Coordinate) -> Self {
    [row, column]
  }
}
impl std::ops::Add <Dimensions> for Coordinate {
  type Output = Coordinate;
  fn add (self, rhs : Dimensions) -> Coordinate {
    (self.row + rhs.rows, self.column + rhs.columns).into()
  }
}
impl std::ops::Sub <Dimensions> for Coordinate {
  type Output = Coordinate;
  fn sub (self, rhs : Dimensions) -> Coordinate {
    (self.row - rhs.rows, self.column - rhs.columns).into()
  }
}
